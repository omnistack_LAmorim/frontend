import React from 'react';
import { Switch, Route} from 'react-router-dom';

import Feed from './pages/Feed';
import New from './pages/New';

//Esse componente não precisa ser escrito em formato de classe, pode ser em formato de função mesmo
function Routes(){
    return (
        <Switch>
            <Route path="/" exact component={Feed} />
            <Route path="/new" exact component={New} />
        </Switch>
    );
}

export default Routes;